# MMM-Garbagezona
Visualizzazione delle prossime date di ritiro carta e rifiuti. 

## Configurazione

### Globale
|Proprietà|Descrizione|
|--- |--- |
|title: String|Titolo da visualizzare nell'intestazione del modulo. Default: Calendario rifiuti|
|showTitle: Boolean|Flag per visualizzare o meno il titolo del modulo. Default: true|
|dateFormat: String|Formato con cui visualizzare le date Default: DD.MM.YYYY|
|nextThreshold: Json|Se attivo, permette la visualizzazione di "prossimo" per quei giorni compresi nell'intervallo definito. Default: {active: true, days: 3}|
|types: Json|Definizione delle tipologie di raccolta, con annesse regole. Default: { "garbage": {...lunedì & venerdì...settimanale...}, "paper": {...mercoledì...bisettimanale...}|


### Tipi di raccolta
|Proprietà|Descrizione|
|--- |--- |
|icon: String|Icona con cui identificare la raccolta (font-awesome friendly). Esempio: fa-scroll|
|weekdays: Integer[]|Giorni della settimana in cui avviene la raccolta (lunedì = 1, domenica = 7). Esempio: [2,5] (martedì, venerdì)|
|frequency: Integer|Cadenza con cui avviene la raccolta. (settimanalmente = 1, ogni due settimane = 2) Esempio: 1|
|first: String|Data della prima occorrenza in cui avviene la raccolta durante l'anno. sempio: '03.01.2020'|
|holidays: Json|Definizione delle date in cui non avviene la raccolta e la loro data sostitutiva. Esempio: { 01.05.2020': '30.04.2020', '08.12.2020': '07.12.2020' }|
|notBefore: Integer|Orario in cui è possibile depositare la spazzatura (il giorno precedente la raccolta). Esempio: 19|
|notAfter: Integer|Orario limite entro in cui è possibile depostare la spazzatura (il giorno della raccolta). Esempio: 7|
