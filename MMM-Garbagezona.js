// MMM-Garbagezona.js

Module.register("MMM-Garbagezona",{

	// Default module config.
	defaults: {
    title: "Calendario rifiuti",
    showTitle: true,
    dateFormat: 'DD.MM.YYYY',
    nextThreshold: {
      active: true,
      days: 3
    },
    types: {
      "garbage": {
        icon: 'fa-trash-alt',
        weekdays: [2,5], // 1 monday, 7 sunday
        frequency: [1], // 1 weekly, 2 fortnightly
        first: '03.01.2020',
        holidays: {
          '01.05.2020': '30.04.2020',
          '08.12.2020': '07.12.2020',
          '25.12.2020': '24.12.2020',
          '01.01.2021': '02.01.2021'
        },
        notBefore: 14,
        notAfter: 7,
        text: ""
      },
      "paper": {
        icon: 'fa-scroll',
        weekdays: [3], // 1 monday, 7 sunday
        frequency: [2], // 1 weekly, 2 fortnightly
        first: '01.01.2020',
        holidays: {},
        notBefore: 20,
        notAfter: 7,
        text: ""
      }
    }
	},

	getStyles: function () {
		return ["MMM-Garbagezona.css", "font-awesome.css"];
  },
  
	getScripts: function() {
		return ["moment.js", "moment-timezone.js"];
	},

	getTemplate: function () {
		return "MMM-Garbagezona.njk";
	},

	getTemplateData: function () {
		return this.config;
	},

  // Override start method.
	start: function () {
    Log.log("Starting module: " + this.name);
    moment.locale(config.language);

    for (var type in this.config.types) {
      var e = this.config.types[type];

      var now = moment();

      var next = this.findNextDate(e.weekdays, e.frequency, e.first);

      var holiday = e.holidays[next.format(this.config.dateFormat)];
      next = (typeof holiday !== 'undefined') ? moment(holiday, this.config.dateFormat) : next;

      if (next.diff(now, 'days') == 0) {
        e.text = "oggi";
        e.text += (now.hour() <= e.notAfter) ? " (esci la spazzatura)" : "";
      } else if (next.diff(now, 'days') == 1) {
        e.text = "domani";
        e.text += (now.hour() >= e.notBefore) ? " (esci la spazzatura)" : "";
      } else {
        var weeks = next.isoWeek() - now.isoWeek();
        e.text = next.format("dddd");

        if (weeks == 1) {
          // Don't say " prossimo" when next is not so far
          if (this.config.nextThreshold.active == false || 
            (this.config.nextThreshold.active == true &&
              next.dayOfYear() - now.dayOfYear() > this.config.nextThreshold.days)) {
                e.text += " prossimo";
          }
        } else if (weeks > 1) {
          e.text += (" fra " + weeks + " settimane");
        }
      }

    }

    this.updateDom();
  },

  findNextDate: function(weekdays, frequency, first) {
    const timemachine = weekdays.map(this.isFuture);
    var firstWeek = moment(first, this.config.dateFormat).isoWeek();
    var isThisWeek = ((moment().isoWeek() - firstWeek)%2 == 0) ? true : false;

    var next = timemachine.find((instant) => {return instant instanceof moment});

    if (next == null) {
      if (frequency == 2 && isThisWeek == true) {
        next = moment().add(2, 'weeks').isoWeekday(weekdays[0]);
      } else {
        next = moment().add(1, 'weeks').isoWeekday(weekdays[0])
      }
    } 

    return next;
  },

  isFuture: function(date) {
    const today = moment().isoWeekday();

    if (today <= date) {
      return moment().isoWeekday(date);
    }

    return false;
  },
  
});
